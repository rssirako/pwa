# pwa

This is app that uses web components, it is using vite and has the following features:

- Component-based architecture is using web components with no dependecies(except for build purposes), which makes it library/framework agnostic and easy to integrate
- The development of components is split into individual files for logic, template and styles.
- Using LESS for styling that gets transpiled to CSS, TS -> JS files. JS and HTML files get minified for prod build.
- Skeleton loading is used for the cards to improve perceived performance and all images are lazy loaded.
- Target of this app are safari browsers on Ipad devices. Testing done on ipad mini,air and pro with dev tools emulator within chrome browser and on safari.
- Implemented client side  caching strategy via a service worker in case ipad is on mobile connection and goes offline.
- For the ease of testing project is deployed here: https://pwa-zeta-jet.vercel.app/


# Setup

```
pnpm install
```

## Dev mode

```
pnpm run start
```

## Build for production

```
pnpm run build