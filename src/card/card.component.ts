import html from './card.component.html?inline';
import css from './card.component.less?inline';
import BaseComponent from "../base/BaseComponent";
import type { BeerItem } from '../types/types';

const template = document.createElement('template');
template.innerHTML = `<style>${css}</style>${html}`;

export const APP_CARD_TAG_NAME = 'app-card';

export class AppCardComponent extends BaseComponent {
    constructor() {
        super(html, css);

    }
    setData(beer: BeerItem): void {
        const beerImageElement = this.shadowRoot?.querySelector('.beer-card__image') as HTMLImageElement | null;
        const beerNameElement = this.shadowRoot?.querySelector('.beer-card__name') as HTMLHeadingElement;
        const beerIbuElement = this.shadowRoot?.querySelector('.beer-card__ibu') as HTMLParagraphElement;

        this.setBackgroundColor(beer.ibu.toString());
        this.setTriangleValue(beer.abv.toString());


        if (
            beerImageElement &&
            beerNameElement &&
            beerIbuElement
        ) {
            beerImageElement.src = beer.image_url ? beer.image_url : 'https://via.placeholder.com/150'; //some of the images get 404 on the vercel server so this will not help in that case
            beerImageElement.alt = beer.name;
            beerNameElement.textContent = this.getDisplayName(beer.name);
            beerIbuElement.textContent = `IBU: ${beer.ibu}`;

        } else {
            console.error('One or more beer card elements are null.');
        }
    }

    setBackgroundColor(ibu: string): void {
        const firstDigit = parseInt(ibu[0]);
        const card = this.shadowRoot?.querySelector('.beer-card') as HTMLImageElement | null;
        const bgColorClass = `ibu-color${firstDigit}`;
        if (card) {
            card.classList.add(bgColorClass);
        }
    }

    setTriangleValue(abv: string): void {
        const triangleEl = this.shadowRoot?.querySelector('.beer-card__triangle') as HTMLImageElement | null;

        if (triangleEl) {
            triangleEl.setAttribute('data-abv', `${abv}%`);
        }
    }

    getDisplayName(name: string): string {
        const nameParts = name.split(' ');

        if (nameParts.length > 1 && (nameParts[0].toLowerCase() === 'the')) {
            return name = `${nameParts[0]} ${nameParts[1]}`;
        }
        return name = nameParts[0];

    }
}

