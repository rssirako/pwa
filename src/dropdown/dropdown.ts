import { APP_DROPDOWN_TAG_NAME, AppDropdownComponent } from './dropdown.component';

if (customElements.get(APP_DROPDOWN_TAG_NAME) === undefined) {
    customElements.define(APP_DROPDOWN_TAG_NAME, AppDropdownComponent);
}
