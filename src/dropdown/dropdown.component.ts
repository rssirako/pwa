import html from './dropdown.component.html?inline';
import css from './dropdown.component.less?inline';
import BaseComponent from "../base/BaseComponent";
import type { DropDown } from '../types/types';

const template = document.createElement('template');
template.innerHTML = `<style>${css}</style>${html}`;

export const APP_DROPDOWN_TAG_NAME = 'app-dropdown';

export class AppDropdownComponent extends BaseComponent {
    constructor() {
        super(html, css);
    }

    generateHtml(options: DropDown[]): void {
        let optionsHtml = '<ul class="order-options" style="display: none;">';
        options.forEach(option => {
            optionsHtml += `<li class="list-options"><button class="list-options-button" value="${option.value}"><span>${option.label}</span><span class="dropdown-icon">${this.getIcon("right")}</span></button>`;
            if (option.suboptions && option.suboptions.length > 0) {
                optionsHtml += '<ul class="order-suboptions" style="display: none;">';
                option.suboptions.forEach(suboption => {
                    optionsHtml += `<li><button value="${suboption.value}">${suboption.label}</button></li>`;
                });
                optionsHtml += '</ul>';
            }
            optionsHtml += '</li>';
        });
        optionsHtml += '</ul>';

        const orderButtonHtml = `<button class="order-button">ORDER <span class="dropdown-icon" >${this.getIcon("down")}</span></button>`;

        const dropdownContent = this.shadowRoot?.querySelector('.dropdown-content');
        if (dropdownContent) {
            dropdownContent.innerHTML = orderButtonHtml + optionsHtml;

            const orderButton = dropdownContent.querySelector('.order-button');
            if (orderButton) {
                orderButton.addEventListener('click', () => {
                    const optionsList = dropdownContent.querySelector('.order-options') as HTMLElement;
                    this.toggleVisibility(optionsList);
                });
            }

            const listOptionsButtons = dropdownContent.querySelectorAll('.list-options-button');
            listOptionsButtons.forEach(button => {
                button.addEventListener('click', () => {
                    const listItem = button.closest('.list-options');
                    if (listItem) {
                        const optionsList = listItem.querySelector('ul');
                        if (optionsList) {
                            this.toggleVisibility(optionsList);
                        }
                    }
                });
            });
        }
    }

    toggleVisibility(element: HTMLElement): void {
        if (element) {
            if (element.style.display === 'block') {
                element.style.display = 'none';
                return;
            }
            element.style.display = 'block';
        }
    }

    getIcon(position: string): string {
        const iconPath = position === 'down' ? '/down-arrow.svg' : '/right-arrow.svg';
        return `<img src="${iconPath}" class="dropdown-icon" alt="dropdown icon">`;
    }

}