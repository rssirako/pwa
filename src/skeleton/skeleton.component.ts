import html from './skeleton.component.html?inline';
import css from './skeleton.component.less?inline';
import BaseComponent from "../base/BaseComponent";

const template = document.createElement('template');
template.innerHTML = `<style>${css}</style>${html}`;

export const APP_SKELETON_TAG_NAME = 'app-skeleton';

export class AppSkeletonComponent extends BaseComponent {
  constructor() {
    super(html, css);
  }
}

