import { APP_SKELETON_TAG_NAME, AppSkeletonComponent } from './skeleton.component';

if (customElements.get(APP_SKELETON_TAG_NAME) === undefined) {
  customElements.define(APP_SKELETON_TAG_NAME, AppSkeletonComponent);
}
