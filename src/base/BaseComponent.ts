export default class BaseComponent extends HTMLElement {
    constructor(htmlTemplate: string, css: string) {
        super();
        const template = document.createElement('template');
        template.innerHTML = `<style>${css}</style>${htmlTemplate}`;
        this.attachShadow({ mode: 'open' });

        if (this.shadowRoot) {
            this.shadowRoot!.appendChild(template.content.cloneNode(true));
        }
    }
}