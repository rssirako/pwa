import { APP_MODAL_TAG_NAME, AppModalComponent } from './modal.component';

if (customElements.get(APP_MODAL_TAG_NAME) === undefined) {
    customElements.define(APP_MODAL_TAG_NAME, AppModalComponent);
}
