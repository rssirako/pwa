import html from './modal.component.html?inline';
import css from './modal.component.less?inline';
import BaseComponent from "../base/BaseComponent";
import type { BeerItem, DropDown } from '../types/types';
import { AppDropdownComponent } from '../dropdown/dropdown.component';
import dropdownValues from '../dropdown/dropdownValues.json';

const optionsData: DropDown[] = dropdownValues;
const template = document.createElement('template');
template.innerHTML = `<style>${css}</style>${html}`;

export const APP_MODAL_TAG_NAME = 'app-modal';

export class AppModalComponent extends BaseComponent {
    constructor() {
        super(html, css);
        this.attachCloseEvent();

    }

    setData(beer: BeerItem): void {
        const modalImageElement = this.shadowRoot?.querySelector('.modal-image') as HTMLImageElement;
        const modalNameElement = this.shadowRoot?.querySelector('.modal-name') as HTMLHeadingElement;
        const modalIbuElement = this.shadowRoot?.querySelector('.modal-ibu') as HTMLParagraphElement;
        const modalAbvElement = this.shadowRoot?.querySelector('.modal-abv') as HTMLParagraphElement;
        const modalDescriptionElement = this.shadowRoot?.querySelector('.modal-description') as HTMLParagraphElement;

        if (
            modalImageElement &&
            modalNameElement &&
            modalIbuElement &&
            modalAbvElement &&
            modalDescriptionElement
        ) {
            modalImageElement.src = beer.image_url ? beer.image_url : 'https://via.placeholder.com/150'; 
            modalImageElement.alt = beer.name;
            modalNameElement.textContent = beer.name;
            modalIbuElement.textContent = `${beer.ibu}`;
            modalAbvElement.textContent = `${beer.abv}%`;
            modalDescriptionElement.textContent = beer.description;

            this.openModal();
            this.generateDropdown();
        } else {
            console.error('One or more modal elements are null.');
        }

    }

    openModal(): void {
        this.style.display = 'block';
        const modalPlaceholder = document.querySelector('.modal-placeholder');
        if (modalPlaceholder) {
            modalPlaceholder.appendChild(this);
        }
        document.body.classList.add('modal-open');
    }

    closeModal(): void {
        this.style.display = 'none';
    }

    attachCloseEvent(): void {
        const closeButton = this.shadowRoot?.querySelector('.close') as HTMLElement | null;
        if (closeButton) {
            closeButton.addEventListener('click', () => {
                this.closeModal();
            });
        }
    }

    generateDropdown(): void {
        const dropDown = document.createElement('app-dropdown') as AppDropdownComponent;
        const dropdownElement = this.shadowRoot?.querySelector('.modal-dropdown-wrapper') as HTMLElement | null;
        const dropDownHtml = dropDown.generateHtml(optionsData);

        if (dropDownHtml !== void 0) {
            dropDown.innerHTML = dropDownHtml;
        }

        if (dropdownElement) {
            dropdownElement.append(dropDown);
        }
    }

}

