import { BeerItem } from '../types/types';

const BEER_DATA_URL = 'https://api.jsonbin.io/v3/b/6630fd9be41b4d34e4ecd1f9';

export async function fetchBeerData(): Promise<BeerItem[]> { 
    try {
        const response = await fetch(BEER_DATA_URL);
        const data = await response.json();
        return  data.record as BeerItem[];
    } catch (error) {
        console.error('Error fetching beer data:', error);
        return []; 
    }
}