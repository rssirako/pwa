export interface BeerItem {
    id: number;
    name: string;
    ibu: number;
    abv: number;
    description: string;
    image_url: string;
}

export interface Metadata {
    id: string,
    private: boolean,
    createdAt: string,
    name: string,
}

export interface BeerApiRaw {
    record: BeerItem[];
    metadata: Metadata;
}

export interface DropDown {
    label: string;
    value: string;
    suboptions?: DropDown[];
}