import { fetchBeerData } from './src/api/getBeers';
import { AppCardComponent } from './src/card/card.component';
import { AppModalComponent } from './src/modal/modal.component';
import { BeerItem, DropDown } from './src/types/types';
import { AppDropdownComponent } from './src/dropdown/dropdown.component';
import { AppSkeletonComponent } from './src/skeleton/skeleton.component';

const SKELETON_CARD_COUNT = 20;
async function getBeers() {
    window.customElements.define('app-card', AppCardComponent);
    window.customElements.define('app-modal', AppModalComponent);
    window.customElements.define('app-dropdown', AppDropdownComponent);

    const result = await fetchBeerData();
    const mainElement = document.querySelector('main');



    if (result.length > 0) {
        const beerCardComponents = result.map((beer: BeerItem) => {


            const beerCard = new AppCardComponent();
            beerCard.setData(beer);
            beerCard.addEventListener('click', () => {
                const modal = document.createElement('app-modal') as AppModalComponent;
                modal.setData(beer);
                document.body.appendChild(modal);
            });
            return beerCard;
        });
        mainElement!.innerHTML = '';
        mainElement!.append(...beerCardComponents);

    } else {
        mainElement!.innerHTML = '<h2 class="service-down-message">Sorry, service is down.</h2>';

    }

}

async function showSkeletonCards() {
    window.customElements.define('app-skeleton', AppSkeletonComponent);
    const mainElement = document.querySelector('main');

    const skeletonCards = Array.from({ length: SKELETON_CARD_COUNT }, () => document.createElement('app-skeleton') as AppSkeletonComponent);
    mainElement!.innerHTML = ''; // Clear existing content
    mainElement!.append(...skeletonCards);
}


window.onload = () => {
    showSkeletonCards();
    getBeers();
    registerSW();
};

function registerSW(){
    if("serviceWorker" in navigator){
        navigator.serviceWorker.register("sw.js").then(registration=>{
          console.log("SW Registered!");
        }).catch(error=>{
          console.log("SW Registration Failed");
        });
    }else{
      console.log("Not supported");
    }
};